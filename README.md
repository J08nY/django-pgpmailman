# django-pgpmailman
[![PyPI](https://img.shields.io/pypi/v/django-pgpmailman.svg)](https://pypi.org/project/django-pgpmailman/) [![build status](https://gitlab.com/J08nY/django-pgpmailman/badges/master/build.svg)](https://gitlab.com/J08nY/django-pgpmailman/commits/master)

A web interface for the Mailman PGP plugin.

## Installation

django-pgpmailman currently requires a development version of mailmanclient.
Doing:
```
pip install django_pgpmailman
```
will pull in the development version of mailmanclient from [J08nY/mailmanclient/plugin](https://gitlab.com/J08nY/mailmanclient/tree/plugin/).

## Configuration

Two example projects are included, one for setting up django-pgpmailman
as a standalone app in a project, the other for setting up together
with Postorius and HyperKitty. You can find the settings for the respective
projects in [single_project](/single_project) and [suite_project](/suite_project).

## License
    
    Copyright (C) 2017 Jan Jancar
    This program is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.
    
    You should have received a copy of the GNU General Public License along with
    this program.  If not, see 
